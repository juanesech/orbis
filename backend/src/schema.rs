diesel::table! {
    products (id) {
        id -> Int4,
        name -> Varchar,
        description -> Text,
        in_stock -> Bool,
    }
}
